# Workspace
Creates workspace EC2 instance.

## Setup
```
terraform init
```
```
terraform plan
```
```
terraform apply
```

## References
- [An Introduction to Terraform](https://blog.gruntwork.io/an-introduction-to-terraform-f17df9c6d180)
- [Resource: aws_instance](https://www.terraform.io/docs/providers/aws/r/instance.html)
- [Amazon EC2 AMI Locator](https://cloud-images.ubuntu.com/locator/ec2/)
- [Amazon EC2 Pricing](https://aws.amazon.com/ec2/pricing/on-demand/)
- [Provisioners](https://www.terraform.io/docs/provisioners/index.html)
