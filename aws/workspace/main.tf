provider "aws" {
  region = "us-west-2"
}

data "aws_ami" "ubuntu" {
  most_recent = true

  filter {
    name   = "name"
    values = ["ubuntu/images/hvm-ssd/ubuntu-bionic-18.04-amd64-server-*"]
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }

  owners = ["099720109477"]
}

resource "aws_instance" "workspace" {
  ami           = data.aws_ami.ubuntu.id
  instance_type = "t2.small"
  key_name = "workspace"
  security_groups = ["workspace"]

  tags = {
    Name = "workspace"
  }

  provisioner "remote-exec" {
    connection {
      type     = "ssh"
      user     = "ubuntu"
      host     = aws_instance.workspace.public_dns
      private_key = file("~/.ssh/workspace.pem")
      agent = true
    }
    inline = [
      # For reasons I'm not sure of this needs to be ran twice before build-essentials is picked up from sources.
      "sudo apt-get update",
      "sudo apt-get update",
      # apt
      "sudo apt-get install -y build-essential",
      "sudo apt-get install -y docker.io",
      "sudo apt-get install -y python3.8",
      "sudo apt-get install -y python3-pip",
      "sudo apt-get install -y git",
      "sudo apt-get install -y yum",
      "sudo apt-get install -y httpie",
      # etc
      "sudo usermod -aG docker $USER",
      # workspace
      "mkdir -p dev/src corp/src",
      "git clone https://gitlab.com/mega-mac-slice/workspace.git dev/src/workspace",
      "cd dev/src/workspace && make install && cd",
    ]
  }
  provisioner "local-exec" {
    command = "echo ${aws_instance.workspace.public_dns}"
  }
  provisioner "local-exec" {
    command = "echo $INSTANCE | jq . > instance.json"
    environment = {
      INSTANCE = jsonencode(aws_instance.workspace)
    }
  }
}
