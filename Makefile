.PHONY: scratch

venv:
	python3 -m venv .venv

backup:
	@./workspace.sh backup

install:
	@./workspace.sh install

scratch:
	@./scripts/scratch
