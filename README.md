# Workspace
Collection of local environment configurations, files and scripts.

## Layout
- _dotfiles_ - Symlinked to `$HOME` and prefixed with a `.`.
- _env_ - Bash environment variables and functions.
- _scripts_ - Scripts directory added to `$PATH`.
- _private_ - Bash environment variables and functions ignored by git.
- _public_ - Bash environment variables and functions.

## Install
```bash
make backup && make install
```

## Tools
The following are a collection of tools that this repo uses or may be useful to you.

\* **required**.

### jq *
[jq](https://stedolan.github.io/jq/) is a lightweight and flexible command-line JSON processor. 

OSX:
```bash
brew install jq
```

### bash 4
OSX:
```bash
brew install bash
```

### bash-completion
OSX:
```bash
brew install bash-completion
```