#!/usr/bin/env bash
# Collection to be ran on a Macbook environment.

function bash_completion() {
  [[ -r /opt/homebrew/etc/profile.d/bash_completion.sh ]] && . /opt/homebrew/etc/profile.d/bash_completion.sh
}

if [ -f /opt/homebrew/bin/brew ]; then
  eval "$(/opt/homebrew/bin/brew shellenv)"
fi

if [[ "$(uname)" == "Darwin" ]]; then
  export PS1='dev@mbp:\w\$'
  bash_completion
  export BASH_SILENCE_DEPRECATION_WARNING=1
fi
