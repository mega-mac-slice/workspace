#!/usr/bin/env bash

# bigger history
export HISTSIZE=100000
export HISTFILESIZE=100000
shopt -s histappend

# default to vim editor
set -o vi
export GIT_EDITOR="vim"
export EDITOR="vim"
export VISUAL="vim"

# colors
alias ls='ls -G'
alias grep='grep --color=always'