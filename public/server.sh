#!/usr/bin/env bash
# Collection to be ran on a server environment.

if [[ $(hostname) == ip-* ]]; then
  export PS1='dev@server:\w\$'
fi