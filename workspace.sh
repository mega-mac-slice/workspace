#!/usr/bin/env bash

WORKSPACEPATH=$(pwd)

function backup {
    backup=~/.profile_backup_"$(date +'%Y_%m_%d')_$(date +%s)"
    echo "backing up ~/.profile to ${backup}"
    cp ~/.profile "${backup}"
}

function create-profile {
    echo "creating ~/.profile"
cat > ~/.profile << EOL

export WORKSPACEPATH=${WORKSPACEPATH}
export PATH="\$PATH:\$WORKSPACEPATH/scripts"
for f in \$WORKSPACEPATH/public/**.sh; do
  [[ -e "\$f" ]] || break
  . "\${f}";
done
for f in \$WORKSPACEPATH/private/**.sh; do
  [[ -e "\$f" ]] || break
  . "\${f}";
done
EOL
    . ~/.profile
}

function dot-files {
    for source in "${WORKSPACEPATH}"/dotfiles/*; do
        dotname=$(basename "${source}")
        link="${HOME}/.${dotname}"
        echo "creating symlink - source: ${source} link: ${link}"
        rm -rf "${link}" && ln -s "${source}" "${link}";
    done
}


function alter-bash-profile {
    echo "altering ~/.bash_profile"

    if  [ -f "${HOME}/.bash_profile" ] && ! grep -q "source ~/.profile"  "${HOME}/.bash_profile"; then
        echo "adding - source ~/.profile"
        echo "source ~/.profile" >> "${HOME}/.bash_profile"
    fi

    if  [ -f "${HOME}/.bashrc" ] && ! grep -q "source ~/.profile"  "${HOME}/.bashrc"; then
        echo "adding - source ~/.profile"
        echo "source ~/.profile" >> "${HOME}/.bashrc"
    fi
}

case "$1" in
        backup)
            backup
            ;;
        install)
            create-profile
            dot-files
            alter-bash-profile
            ;;
        scratch)
            scratch
            ;;
        *)
            echo $"Usage: $0 {backup|install|scratch}"
            exit 1
esac
